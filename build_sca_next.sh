#!/usr/bin/env bash

source setup_paths.sh
echo You need to provide a valid FELIX_ROOT which should contain the dependancies. Current FELIX_ROOT is ${FELIX_ROOT}
rm -Rf build
mkdir build
cd build
cmake ../ -DSTANDALONE_SCA_SOFTWARE=1 -DHAVE_NETIO_NEXT=1 -DHAVE_NETIO=1 -DSTANDALONE_WITH_EVENT_RECORDER=OFF -DCMAKE_BUILD_TYPE=Debug
NUM_JOBS=$((2*`nproc`))
make -j $NUM_JOBS || exit 1
cd ..
echo "Built standalone ScaSoftware with netio-next"
