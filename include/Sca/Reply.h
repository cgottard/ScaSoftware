/*
 * Reply.h
 *
 *  Created on: May 5, 2016
 *      Author: pnikiel
 */

#ifndef SCA_REPLY_H_
#define SCA_REPLY_H_

#include <Hdlc/Payload.h>
#include <initializer_list>

namespace Sca
{

class Reply: public Hdlc::Payload
{
public:
	Reply( const Hdlc::Payload& other);
	Reply( uint8_t channel, uint8_t error, std::initializer_list<uint8_t> data );

	void assignTransactionId (uint8_t transactionId);
	uint8_t transactionId() const { return m_data[0]; }


	uint8_t channelId() const { return m_data[1]; }
	uint8_t error() const { return m_data[2]; }

	std::string toString() const;



};

}

#endif /* SCA_REPLY_H_ */
