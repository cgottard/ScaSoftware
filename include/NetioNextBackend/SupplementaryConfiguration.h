/*
 * SupplementaryConfiguration.h
 *
 *  Created on: Jan 22, 2021
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 */

#pragma once

// TODO: The following definitions shall be exposed as supplementary config structure
#define FELIX_LOG_LEVEL "fatal"
#define FELIX_BUS_DIR "/afs/cern.ch/work/c/cgottard/FELIX/bus"
#define FELIX_BUS_GROUP_NAME "FELIX"
#define FELIX_NETIO_PAGES "128"
#define FELIX_NETIO_PAGESIZE "64"
#define FELIX_VERBOSE_BUS "False"
#define FELIX_VERBOSE_ZYRE "False"