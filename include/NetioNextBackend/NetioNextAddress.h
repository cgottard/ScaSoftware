/*
 * 	NetioNextAddress.h
 *
 *  Created on: Jan 22, 2021
 *      Author: Paris Moschovakos
 */

#pragma once

#include <string>

namespace NetioNextBackend
{

class NetioNextAddress
{
public:
	NetioNextAddress (const std::string& stringAddress);

	std::string toString() const { return m_originalAddress; }

	std::string felixHostname() const { return m_felixHostname; }
	std::string felixbusAddess() const { return m_felixbusAddress; }
	uint64_t fidTx() const { return m_fidTx; }
	uint64_t fidRx() const { return m_fidRx; }

	// Probably move them since it will be cumbersome to come from the sca address
	std::string felixClientLogLevel() const { return m_felixClientLogLevel; }
	std::string felixClientVerboseBus() const { return m_felixClientVerboseBus; }
	std::string felixClientVerboseZyre() const { return m_felixClientVerboseZyre; }

private:
	std::string m_originalAddress;
	std::string m_felixHostname;
	std::string m_felixbusAddress;
	uint64_t m_fidTx;
	uint64_t m_fidRx;
	std::string m_felixClientLogLevel;
	std::string m_felixClientVerboseBus;
	std::string m_felixClientVerboseZyre;

};

}