#pragma once

#include <memory>
#include <list>
#include <mutex>
#include <condition_variable>

#include <Hdlc/Backend.h>
#include <Hdlc/Payload.h>
#include <Hdlc/LocalStatistician.h>
#include <NetioNextBackend/NetioNextAddress.h>

#include <hdlc_coder/hdlc.hpp>
#include <felix/felix_client_thread.hpp>

namespace NetioNextBackend
{

class HdlcBackend : public Hdlc::Backend
{
public:
	HdlcBackend (const std::string& netioNextAddress);
	virtual ~HdlcBackend ();

	void send (const Hdlc::Payload &request ) final;
    void send (
              const std::vector<Hdlc::Payload> &requests,
              const std::vector<unsigned int> times
          ) final;

	void sendHdlcControl (uint8_t hdlcControl) final;
	void cleanUp ();

	void on_init();
	void on_connect(uint64_t fid);
    void on_disconnect(uint64_t fid);
	void on_data(uint64_t fid, const uint8_t* data, size_t size, uint8_t status);

	void subscribeReceiver ( ReceiveCallBack f ) final { m_receivers.push_back(f); }
	const std::string& getAddress () const final { return m_originalNetioNextAddress; }
	virtual Hdlc::BackendStatistics getStatistics () const { return m_statistician.toBackendStatistics(); }

private:
	std::string m_originalNetioNextAddress;
	std::shared_ptr<const NetioNextAddress> m_netioNextAddress;
	std::list<ReceiveCallBack> m_receivers;
	Hdlc::LocalStatistician m_statistician;

	unsigned int m_seqnr; // HDLC sequence number
    bool m_clientSubscribed;
	std::mutex m_subscribeSynchronizationMutex;
	std::condition_variable m_subscribedConditionVariable;

	std::mutex m_transmitSynchronizationMutex;

	FelixClientThread::OnDataCallback m_felixConfigOnData;
	FelixClientThread::OnInitCallback m_felixConfigOnInit;
	FelixClientThread::OnConnectCallback m_felixConfigOnConnect;
	FelixClientThread::OnDisconnectCallback m_felixConfigOnDisconnect;
	FelixClientThread::Properties m_felixConfigProperties;
	FelixClientThread::Config m_felixConfig;
	FelixClientThread m_felixClient;

};


}
