/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018.
 */

/* -------------------------------------------------------------------------
File   : BitFile.h

Descr  : FPGA bit-file (.bit) class.

History: 03NOV17; Created.
---------------------------------------------------------------------------- */

#ifndef BITFILE_H
#define BITFILE_H

#include <string>
#include <vector>

namespace HenksJtag
{
  typedef unsigned char byte;

  class BitFile
  {
  public:

    BitFile();

    ~BitFile();

    byte*        data       ( ) { return _data; }
    unsigned int nbytes     ( ) { return _nbytes; }

    unsigned int fieldCount ( ) { return _field.size(); }
    int          fieldId    ( unsigned int index );
    int          fieldLength( unsigned int index );
    byte*        fieldData  ( unsigned int index );

    void setFile            ( const std::string &filename,
                              bool skip_data = false );
    void setBlob            ( const std::string& blob );

    std::string fileName    ( ) { return _fileName; }
    bool valid              ( ) { return _valid; }

    void reverseBytes       ( );

  private:
    bool        _valid;
    std::string _fileName;

    typedef struct field
    {
      int   id;
      int   length;
      char *data;
    } field_t;

    std::vector<field_t> _field;
    byte                *_data;
    unsigned int         _nbytes;
  };

  class BitfileException: public std::runtime_error
  {
  public:
    BitfileException( const std::string &str );

    ~BitfileException();

    std::string toString();

  private:
    std::string _str;
    int         _data;
  };
}
#endif // BITFILE_H
