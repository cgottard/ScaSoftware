/*
 * ADC.h
 *
 *  Created on: May 4, 2016
 *      Author: pnikiel
 */

#ifndef SCASIMULATOR_ADC_H_
#define SCASIMULATOR_ADC_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class ADC: public ScaChannel
{
public:

	ADC( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~ADC ();

	virtual void onReceive( const Sca::Request &request );
private:
	uint8_t  m_currentChannel;
	uint32_t m_currentSourceMask;
};

}

#endif /* SCASIMULATOR_ADC_H_ */
