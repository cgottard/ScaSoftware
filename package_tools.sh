#!/usr/bin/env bash

echo "Package tools: trying to create a package for commit id $1"
DIR=ScaSoftwareTools-$1
rm -Rf $DIR $DIR".tar.gz" 
mkdir $DIR 
cp build/Demonstrators/Timing/timing build/Demonstrators/SimulatorOverNetIo/simulator_netio build/Demonstrators/StandaloneI2cSimulation/standalone_i2c build/Demonstrators/GbtxConfiguration/gbtx_configuration build/Demonstrators/HenksFScaJtag/fscajtag build/Demonstrators/PingSca/ping_sca build/Demonstrators/StandaloneAdcSimulation/standalone_adc build/Demonstrators/StandaloneDacSimulation/standalone_dac build/Demonstrators/StandaloneSpiSimulation/standalone_spi Demonstrators/README.md $DIR 
tar cf $DIR".tar" $DIR 
gzip -9 $DIR".tar"  
