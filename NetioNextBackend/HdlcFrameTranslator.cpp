/*
 * HdlcFrameTranslator.cpp
 *
 *  Created on: Oct 22, 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *
 *      Description : Based on specifications of ePort HDLC IP core used in SCA
 *      https://espace.cern.ch/GBT-Project/GBT-SCA/Manuals/ePort_HDLC_specs.pdf
 */

#include <NetioNextBackend/HdlcFrameTranslator.h>
#include <NetioNextBackend/HdlcBackendCommon.h>
#include <ScaCommon/ScaSwLogComponents.h>

using Sca::LogComponentLevels;

namespace NetioNextBackend
{

constexpr uint8_t UNNUMBERED_ACKNOWLEDGE = 0x63;
constexpr uint8_t TEST_REPLY = 0xE3;

HdlcFrameTranslator::HdlcFrameTranslator (
        const std::vector<uint8_t>& hdlcFrame,
        std::shared_ptr<const NetioNextAddress>& netioNextAddress) :
		m_address{hdlcFrame.at(0)},
		m_control{hdlcFrame.at(1)},
		m_range{},
		m_fcs{},
		m_isFcsValid{true},
		m_hdlcFrameTypeSize{hdlcFrame.size()},
		m_netioNextAddress(netioNextAddress)
{

	switch(m_hdlcFrameTypeSize) {
	    case HdlcFrameTypeSize::HDLC_U_FRAME_SIZE : {
				LOG(Log::DBG, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : Incoming 4-byte reply: 0x " << vectorAsHex(hdlcFrame);
				m_fcs = ((uint16_t)hdlcFrame[3] << 8) | hdlcFrame[2];
				break;
	    }

	    case HdlcFrameTypeSize::HDLC_SREJ_FRAME_SIZE : {
				LOG(Log::DBG, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : Incoming 6-byte reply: 0x " << vectorAsHex(hdlcFrame);
				m_range = ((uint16_t)hdlcFrame[3] << 8) | hdlcFrame[2];
				m_fcs = ((uint16_t)hdlcFrame[5] << 8) | hdlcFrame[4];
				break;
	    }

	    default: {
				LOG(Log::ERR, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : Incoming reply of unknown size: 0x " << vectorAsHex(hdlcFrame);
				break;
	    }
	}

	if (!validateFcs(hdlcFrame))
		m_isFcsValid = false;

	validateAddress();
	validateControlRange();

}

bool HdlcFrameTranslator::validateAddress ()
{
	if (getAddress() == 0xff)
	{
		LOG(Log::DBG, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : The address of the incoming frame is valid";
		return true;
	}
	LOG(Log::ERR, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : The address in this HDLC frame is invalid";
	return false;
}

bool HdlcFrameTranslator::validateControlRange ()
{
	if (m_hdlcFrameTypeSize == HdlcFrameTypeSize::HDLC_U_FRAME_SIZE)
	{
		if (m_control == UNNUMBERED_ACKNOWLEDGE)
		{
			LOG(Log::WRN, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : reply to HDLC RESET/CONNECT frame";
			return true;
		}
		else if (m_control == TEST_REPLY)
		{
			LOG(Log::WRN, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : reply to HDLC TEST frame";
			return true;
		}

		THROW_WITH_ORIGIN(std::runtime_error, "The control field in this HDLC frame doesn't correspond to a known SCA reply");
		return false;
	}
	else if (m_hdlcFrameTypeSize == HdlcFrameTypeSize::HDLC_SREJ_FRAME_SIZE)
	{

		constexpr unsigned int startNsMask = 0x000E;
		constexpr unsigned int endNsMask = 0x00E0;

		unsigned int startNs = (m_range & startNsMask) >> 1;
		unsigned int endNs = (m_range & endNsMask) >> 5;

		unsigned int missingRequests;
		constexpr unsigned int includeZeroInSubtraction = 1;
		constexpr unsigned int rollOverModulo8 = 8;

		if ( startNs > endNs )
			missingRequests = rollOverModulo8 + (endNs - startNs + includeZeroInSubtraction);
		else
			missingRequests = endNs - startNs + includeZeroInSubtraction;

		LOG(Log::ERR, LogComponentLevels::netioNext()) << "SCA at " << m_netioNextAddress->toString() << " : Missing requests with sequence number from "
				<< std::hex << startNs << " to " << std::hex << endNs
				<< ". (total=" << missingRequests << ")";
		return true;
	}
	else // forward any other packet
		return true;

	return false;
}

}
