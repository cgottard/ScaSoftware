/*	
 * NetioNextAddress.cpp
 *
 *  Created on: Jan 22, 2021
 *      Author: Paris Moschovakos
 */

#include <boost/regex.hpp>

#include <NetioNextBackend/NetioNextAddress.h>
#include <ScaCommon/except.h>

namespace NetioNextBackend
{

NetioNextAddress::NetioNextAddress (const std::string& stringAddress):
		m_originalAddress(stringAddress),
		m_fidTx(0),
		m_fidRx(0)
{
	// Address concept idea attributed to Piotr
	boost::regex fidAddressRegex( "^fid\\/"
			"(?<felixbus>[a-zA-Z0-9][a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)\\/"
			"(?<fid_to_sca>(0[xX])?[0-9a-fA-F]+)\\/"
			"(?<fid_from_sca>(0[xX])?[0-9a-fA-F]+)"
			"$"
			);
	boost::smatch matchResults;

	bool matched = boost::regex_match( stringAddress, matchResults, fidAddressRegex );
	if (!matched)
	{
		THROW_WITH_ORIGIN(std::runtime_error,
				"supplied netio-next address didn't match the regex for fid address. Given string was: '"+
				stringAddress+"' Here is an example: 'fid/10.193.16.116/1010000000013f00/101000000001bf00'");
	}
	m_felixbusAddress = matchResults["felixbus"];

	m_fidTx = std::stoul(matchResults["fid_to_sca"], nullptr, 16);
	m_fidRx = std::stoul(matchResults["fid_from_sca"], nullptr, 16);

}

}



