#include <boost/thread/locks.hpp>
#include <Hdlc/BackendFactory.h>

#include <ScaCommon/except.h>

#include <ScaSimulator/HdlcBackend.h>

#ifdef HAVE_NETIO
#include <NetIoSimpleBackend/HdlcBackend.h>
#endif

#ifdef HAVE_NETIO_NEXT
#include <NetioNextBackend/HdlcBackend.h>
#endif

#include <LogIt.h>
#include <ScaCommon/ScaSwLogComponents.h>

namespace Hdlc
{

   using std::string;
/**
 * @return Hdlc::Backend *
 * @param  scaAddress
 *
 * The typical format for scaAddress is:    backend-type://address
 * backend-type is one of sca-simulator, felix, ...   and address is specific to backend-type
 */
    Backend * BackendFactory::getBackend (const std::string& scaAddress )
    {
	//BackendFactory might be concurrently used from multiple threads
	boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);

	// maybe this backend was already opened? protect from getting twice to the same backend.
	if (m_openedBackends.count(scaAddress))
	{
            scasw_throw_runtime_error_with_origin("This backened is already opened by another SCA object. You probably have address duplication.");
	}
	else
	{  // not in cache - have to create it
            string separator ("://");

            size_t firstMatch = scaAddress.find( separator );
            if (firstMatch == string::npos)
                scasw_throw_runtime_error_with_origin("malformed address: no separator '"+separator+"'");

            string backendType = scaAddress.substr(0, firstMatch);
            string specificAddress = scaAddress.substr(firstMatch + separator.length());

            LOG(Log::DBG, ::Sca::LogComponentLevels::backendFactory())  << "backendType='" << backendType << "' specificAddress='" << specificAddress << "'";

            Backend * obtainedBackend = 0;

            if (backendType == "sca-simulator")
            {
                obtainedBackend = new ScaSimulator::HdlcBackend( specificAddress );
            }
	    else
#ifdef HAVE_NETIO
            if (backendType == "simple-netio")
            {
                obtainedBackend = new NetIoSimpleBackend::HdlcBackend( specificAddress );
            }
	    else
#endif // HAVE_NETIO
#ifdef HAVE_NETIO_NEXT
            if (backendType == "netio-next")
            {
                obtainedBackend = new NetioNextBackend::HdlcBackend( specificAddress );
            }
	    else
#endif // HAVE_NETIO_NEXT
                scasw_throw_runtime_error_with_origin("backend type is unknown: '"+backendType+"'");

            m_openedBackends.insert( std::pair<std::string, Backend*> ( scaAddress, obtainedBackend ) );
            return obtainedBackend;

	}
    }

    /* You should call this from your backend destructor */
    void BackendFactory::unregister(Backend* backend)
    {
        boost::lock_guard<decltype(m_accessLock)> lock (m_accessLock);
        decltype(m_openedBackends)::iterator it;
        it = std::find_if(m_openedBackends.begin(), m_openedBackends.end(), [backend](const decltype(m_openedBackends)::value_type& x) {return x.second == backend;});
        if (it != m_openedBackends.end())
        {
            m_openedBackends.erase(it);
        }
    }

boost::recursive_mutex BackendFactory::m_accessLock;
std::map<std::string, Backend*> BackendFactory::m_openedBackends;



}
