#!/bin/sh


rm -Rf build
mkdir build
cd build
cmake ../ -DSTANDALONE_SCA_SOFTWARE=1 -DHAVE_NETIO=1 -DSTANDALONE_WITH_EVENT_RECORDER=ON -DCMAKE_BUILD_TYPE=Debug
make -j `nproc` || exit 1
cd ..
echo "Build was made in build/ directory"
