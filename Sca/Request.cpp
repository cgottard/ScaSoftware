/*
 * Request.cpp
 *
 *  Created on: Mar 14, 2017
 *      Author: pnikiel
 */

#ifndef SCA_REQUEST_CPP_
#define SCA_REQUEST_CPP_

#include <Sca/Request.h>

namespace Sca
{

Request::Request( uint8_t channel, uint8_t command, std::initializer_list<uint8_t> data ):
	Hdlc::Payload( {0, channel, (uint8_t)(data.size()), command}, data ),
	m_trailingPause(0)
{

}

Request::Request( uint8_t channel, uint8_t command, uint8_t* data, uint8_t length ):
	Hdlc::Payload( {0, channel, (uint8_t)length, command}, data, length ),
	m_trailingPause(0)
{

}

void Request::assignTransactionId (uint8_t transactionId)
{
	m_data[0] = transactionId;
}



}



#endif /* SCA_REQUEST_CPP_ */
