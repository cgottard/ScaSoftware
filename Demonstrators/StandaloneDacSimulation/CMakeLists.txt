add_executable(standalone_dac 
  main.cpp
  ${DEMONSTRATORS_COMMON_OBJS}
  )

target_link_libraries(standalone_dac ${DEMONSTRATORS_COMMON_LIBS} )

