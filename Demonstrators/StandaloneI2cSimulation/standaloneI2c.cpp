/**
 * Standalone I2C Tool
 *
 *  Created on: April 8, 2017
 *      Author: Paris Moschovakos
 * Description: Standalone program to read/write on the I2C interface of the SCA.
 * 				Allows for read/write directly to an I2C device or in a register
 * 				in their internal address space.
 *
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>
#include <iomanip>
#include <DemonstratorCommons.h>

using std::vector;

struct Config
{
    std::string         address;
    int					i2cMaster;
    int					frequency;
    bool				sclPadCmosOutput;
    std::string			addressingMode;
    int					i2cDevice;
    std::string			data;
    int					nBytes;
    int					registerAddress;
    bool				isRead = false;
    bool				isWrite = false;
    bool 				hasInternalAddressSpace = false;

};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
	std::string logLevelStr;
    options_description options("Options");
    options.add_options()
	("help,h", 				"show help")
	("address,a",			value<std::string>(&config.address)->default_value("simple-netio://direct/pcatlnswfelix01.cern.ch/12340/12350/BF"),  helpForAddress )
	("I2C master,i",		value<int>(&config.i2cMaster), "I2C master [0..15]")
	("I2C device,d",		value<int>(&config.i2cDevice), "I2C device address")
	("register,g",			value<int>(&config.registerAddress), "Register internal address (2 byte address space)")
	("read,r",				value<int>(&config.nBytes), "I2C read amount of bytes [1..16]")
	("write,w",				value<std::string>(&config.data), "I2C write value in hex (e.g. A1B2C3D4E5F6)")
	;
    options_description moreOptions("More options");
    moreOptions.add_options()
	("Addressing mode,m",	value<std::string>(&config.addressingMode)->default_value("7bit"), "I2C addressing mode: 7bit/10bit")
	("SCL mode,s",			value<bool>(&config.sclPadCmosOutput)->default_value(0), "SCL mode: open-drain/CMOS output: 0/1")
	("Frequency,f",			value<int>(&config.frequency)->default_value(100), "Bus speed: 100, 200, 400, 1000 (KHz)")
	("Trace level,t", 		value<std::string>(&logLevelStr)->default_value("WRN"), "Trace level: ERR,WRN,INF,DBG,TRC")
	;

    options_description cmdLineOptions;
    cmdLineOptions.add(options).add(moreOptions);

    variables_map vm;
    store( parse_command_line (argc, argv, cmdLineOptions), vm );
    notify (vm);
    if (vm.count("help"))
    {
    	std::cout << "Usage: standalone_i2c [OPTION]... [MORE_OPTIONS]... -r 4 \n";
    	std::cout << "  or   standalone_i2c [OPTION]... [MORE_OPTIONS]... -w 01020304 \n";
    	std::cout << "  or   standalone_i2c [OPTION]... [MORE_OPTIONS]... -g 0 -r 4 \n";
    	std::cout << "  or   standalone_i2c [OPTION]... [MORE_OPTIONS]... -g 0 -w 01020304 \n\n";
    	std::cout << options << std::endl;
    	std::cout << moreOptions << std::endl;
    	exit(1);
    }
    if ( config.i2cMaster > 15 || config.i2cMaster < 0 )
        {
    	std::cout << "I2C channel requested is out of SCA range: '" << (int)config.i2cMaster << "'" << std::endl;
    	exit(1);
        }
    else
    {
    	config.i2cMaster += Sca::Constants::I2c::I2C_CHANNEL_ID_OFFSET;
    }
    initializeLogging(logLevelStr);
    if ( vm.count("read") && !vm.count("write") )
	{
		config.isRead = true;
		if ( 0 > config.nBytes && config.nBytes < 16 )
		{
			std::cout << "Number of data requested is out of SCA range: '" << (int)config.nBytes << "'" << std::endl;
			exit(1);
		}
	}
    if ( vm.count("write") )
	{
		config.isWrite = true;
	}
    if ( vm.count("register") )
	{
    	if ( config.registerAddress > 65535 )
		{
			throw std::out_of_range("Internal register address is out of range: " + std::to_string(config.registerAddress));
			exit(1);
		}
		config.hasInternalAddressSpace = true;
	}

    return config;
}

int main (int argc, char* argv[])
{
	Config config = parseProgramOptions(argc, argv);

	Sca::Sca sca ( config.address );

	uint16_t busSpeed = (uint16_t)(config.frequency);
	bool addressingMode = ( config.addressingMode=="10bit" ) ? true : false;
	int greaterThan255 = ( config.registerAddress > 255 ) ? 1 : 0;
	std::vector<uint8_t> registerAddress = { (uint8_t)(config.registerAddress-greaterThan255*256), (uint8_t)greaterThan255 };

	try
	{

		uint8_t controlRegister = sca.i2c().getControlRegister( config.i2cMaster );
		LOG(Log::TRC) << "Initial: Control register: 0x" << std::hex << (int)controlRegister;
		sca.i2c().setFrequency( config.i2cMaster, busSpeed );
		sca.i2c().setSclMode( config.i2cMaster, config.sclPadCmosOutput );
		controlRegister = sca.i2c().getControlRegister( config.i2cMaster );
		LOG(Log::TRC) << "Reconfigured: Control register: 0x" << std::setbase(16) << (int)controlRegister;

		/*
		 * Write stand-alone
		 *
		 * */
		if ( config.isWrite ) {

			std::vector<uint8_t> value;

			if ( config.data.size() > 32 || (config.data.size() > 28 && config.hasInternalAddressSpace) )
			    throw std::out_of_range( "Argument out of range. Too many hex digits. " );
			if ( config.data.size() % 2 != 0 )
			    throw std::out_of_range( "Argument out of range. Odd number of hex values. " );

			for ( std::size_t i = 0; i < (config.data.length() / 2); ++i )
			{
				value.push_back((uint8_t)(std::stoul( config.data.substr( i*2, 2), 0, 16)));
			}

			LOG(Log::WRN) << "Data to be written:";
			for ( auto i = value.begin(); i != value.end(); ++i )
				LOG(Log::WRN) << "0x" << std::hex << (int)*i;

			if ( !config.hasInternalAddressSpace )
			{
				LOG(Log::TRC) << "Direct I2C write";

				uint8_t reply = sca.i2c().write( config.i2cMaster, addressingMode, config.i2cDevice, value );
				auto statusflags = sca.i2c().getStatus( config.i2cMaster, reply );
				LOG(Log::WRN) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

			}
			else
			{
				LOG(Log::TRC) << "Write to an internal register";

				std::vector<uint8_t> registerAddressValue ( registerAddress );
				registerAddressValue.insert( std::end( registerAddressValue ), std::begin( value ), std::end( value ));

				uint8_t reply = sca.i2c().write( config.i2cMaster, addressingMode, config.i2cDevice, registerAddressValue );
				auto statusflags = sca.i2c().getStatus( config.i2cMaster, reply );
				LOG(Log::WRN) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

			}

			LOG(Log::WRN) << "Write done!";

		}

		/*
		 * Read stand-alone
		 *
		 * */
		if ( config.isRead ) {

			uint8_t nBytes = (uint8_t)config.nBytes;

			if ( !config.hasInternalAddressSpace )
			{
				LOG(Log::TRC) << "Direct I2C read of " << (int)nBytes << "bytes";

				std::vector<uint8_t> reply = sca.i2c().read( config.i2cMaster, addressingMode, config.i2cDevice, nBytes );
				auto statusflags = sca.i2c().getStatus( config.i2cMaster, reply[0] );
				LOG(Log::WRN) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				LOG(Log::WRN) << "I2C device value: ";
				for ( auto i = reply.begin() + 1; i != reply.begin() + 1 + config.nBytes; ++i )
					LOG(Log::WRN) << "0x" << std::hex << (int)*i;
				}
			else
			{
				LOG(Log::TRC) << "Read an internal register of " << (int)nBytes << "bytes";

				uint8_t writeAddr = sca.i2c().write( config.i2cMaster, addressingMode, config.i2cDevice, registerAddress );
				auto statusflags = sca.i2c().getStatus( config.i2cMaster, writeAddr );
				LOG(Log::WRN) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				std::vector<uint8_t> reply = sca.i2c().read( config.i2cMaster, addressingMode, config.i2cDevice, nBytes );
				statusflags = sca.i2c().getStatus( config.i2cMaster, reply[0] );
				LOG(Log::WRN) << "successfulTransaction: " << std::boolalpha << statusflags.successfulTransaction;
				LOG(Log::TRC) << "sdaLineLevelError: " << std::boolalpha << statusflags.sdaLineLevelError;
				LOG(Log::TRC) << "invalidCommandSent: " << std::boolalpha << statusflags.invalidCommandSent;
				LOG(Log::TRC) << "lastOperationNotAcknowledged: " << std::boolalpha << statusflags.lastOperationNotAcknowledged;

				LOG(Log::WRN) << "Register " << config.registerAddress << " value: ";
				for ( auto i = reply.begin() + 1; i != reply.begin() + 1 + config.nBytes; ++i )
					LOG(Log::WRN) << "0x" << std::hex << (int)*i;

			}

		}
	}
	catch (std::exception &e)
	{
		LOG(Log::ERR) << "exception " << e.what();
	}


}
