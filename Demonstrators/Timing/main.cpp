/*
 * main.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: pnikiel
 */

#include <Hdlc/BackendFactory.h>
#include <Sca/Defs.h>
#include <Sca/SynchronousService.h>
#include <Sca/Sca.h>
#include <LogIt.h>
#include <LogLevels.h>
#include <unistd.h>
#include <iostream>
#include <sys/time.h>
#include <stdlib.h>
#include <stdexcept>
#include <boost/program_options.hpp>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/weighted_mean.hpp>
#include <boost/accumulators/statistics/moment.hpp>
#include <boost/accumulators/statistics/variance.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <chrono>

#include <unordered_map>

#include <DemonstratorCommons.h>

using namespace boost::accumulators;

struct Config
{
    std::string          address;
    int                  number;
};

enum OperationType
{
	ADC_SET_INPUT,
	ADC_GO,
	GPIO_GET_REGISTER,
	SPI_WRITE_128BITS
};

Config parseProgramOptions(int argc, char* argv[])
{
    using namespace boost::program_options;
    using namespace ScaSwDemonstrators;
    Config config;
    options_description options;
    std::string logLevelStr;
    options.add_options()
	("help,h", "show help")
	("address,a",     value<std::string>(&config.address)->default_value("sca-simulator://1"),  helpForAddress )
	("trace_level,t", value<std::string>(&logLevelStr)->default_value("INF"),                   "Trace level, one of: ERR,WRN,INF,DBG,TRC")
	("iterations,i",  value<int>(&config.number)->default_value(1000),                          "How many iterations")
	;
    variables_map vm;
    store( parse_command_line (argc, argv, options), vm );
    notify (vm);
    if (vm.count("help"))
    {
	std::cout << options << std::endl;
	exit(1);
    }
    initializeLogging(logLevelStr);
    
    return config;
}

void invokeOperation( Sca::Sca& sca, OperationType type )
{
	switch (type)
	{
	case ADC_SET_INPUT:
		sca.adc().setInputLine(1);
		break;
	case ADC_GO:
		sca.adc().go();
		break;
	case GPIO_GET_REGISTER:
		sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataIn);
		break;
	case SPI_WRITE_128BITS:
		{
			std::vector<uint8_t> fakedata ( 16, 0x69);
			sca.spi().writeSlave(0, fakedata );

		}
	break;
	}
}

std::string opToString( OperationType op)
{
	switch (op)
	{
	case ADC_SET_INPUT: return "ADC_SET_INPUT";
	case GPIO_GET_REGISTER: return "GPIO_GET_REGISTER";
	case SPI_WRITE_128BITS: return "SPI_WRITE_128BITS";
	case ADC_GO: return "ADC_GO";
	default: return "???";
	}
}

struct Result
{
	double avg;
	double std;
};

Result measureOperations( Sca::Sca& sca, OperationType optype, int number)
{


    accumulator_set<double, stats<tag::variance> > acc;
    for (int i=0; i<number; ++i)
    {
    	auto t1 = std::chrono::high_resolution_clock::now();
    	invokeOperation(sca, optype);
        auto t2 = std::chrono::high_resolution_clock::now();
        double us = std::chrono::duration_cast< std::chrono::microseconds> ( t2 - t1 ).count();
        acc( us );

    }
    Result res;
    res.avg = mean(acc);
    res.std = sqrt(variance(acc));
    return res;
}

int main (int argc, char* argv[])
{
    Config config = parseProgramOptions( argc, argv);
    srand(time(0));

    Sca::Sca sca ( config.address ) ;
    sca.spi().setTransmissionBaudRate(20000000);
    sca.spi().setTransmisionBitLength(128);
    sca.spi().setSsMode(false);




    std::map<OperationType, Result> results;
    std::list<OperationType> ops;
    ops.push_back(ADC_SET_INPUT);
    ops.push_back(GPIO_GET_REGISTER);
    ops.push_back(SPI_WRITE_128BITS);
    ops.push_back(ADC_GO);

    for (auto op : ops)
    	results.emplace( op,  measureOperations( sca, op, config.number) );


    for (auto op : ops)
    	std::cout << opToString(op) << " For " << config.number << " samples, on average it took: " << results[op].avg << " us std " << results[op].std << " us " << std::endl;



}
