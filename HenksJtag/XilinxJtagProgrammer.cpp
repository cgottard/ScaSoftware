/*
 * XilinxJtagProgrammer.cpp
 *
 *  Created : 18 Jun 2018
 *  Author  : pnikiel
 *  Extended: Dec 2019, henkb
 */

#include <HenksJtag/XilinxJtagProgrammer.h>
#include <HenksJtag/BitFile.h>

using namespace std;

namespace HenksJtag
{
// Xilinx VIRTEX7 JTAG instructions
#define V7_IRLEN        6
#define V7_IDCODE_LEN   32
#define V7_CFG_OUT      0x04
#define V7_CFG_IN       0x05
#define V7_IDCODE       0x09
#define V7_JPROGRAM     0x0B
#define V7_JSTART       0x0C
#define V7_JSHUTDOWN    0x0D
#define V7_ISC_ENABLE   0x10
#define V7_ISC_PROGRAM  0x11
#define V7_ISC_DISABLE  0x16
#define V7_BYPASS       0x3F

const string V7_STATBITS_STR[] = {
  "CRC ERROR                     ",
  "DECRYPTOR ENABLE              ",
  "PLL LOCK STATUS               ",
  "DCI MATCH STATUS              ",
  "END OF STARTUP (EOS) STATUS   ",
  "GTS_CFG_B STATUS              ",
  "GWE STATUS                    ",
  "GHIGH STATUS                  ",
  "MODE PIN M[0]                 ",
  "MODE PIN M[1]                 ",
  "MODE PIN M[2]                 ",
  "INIT_B INTERNAL SIGNAL STATUS ",
  "INIT_B PIN                    ",
  "DONE INTERNAL SIGNAL STATUS   ",
  "DONE PIN                      ",
  "IDCODE ERROR                  ",
  "SECURITY ERROR                ",
  "SYSTEM MON OVERTEMP ALARM STAT",
  "CFG STARTUP STATEMACHINE PHASE",
  "CFG STARTUP STATEMACHINE PHASE",
  "CFG STARTUP STATEMACHINE PHASE",
  "RESERVED                      ",
  "RESERVED                      ",
  "RESERVED                      ",
  "RESERVED                      ",
  "CFG BUS WIDTH DETECTION       ",
  "CFG BUS WIDTH DETECTION       ",
  "HMAC ERROR                    ",
  "PUDC_B PIN                    ",
  "BAD PACKET ERROR              ",
  "CFGBVS PIN                    ",
  "RESERVED                      "
};

// ----------------------------------------------------------------------------

XilinxJtagProgrammer::XilinxJtagProgrammer( Sca::Sca& sca,
                                            unsigned int freqMhz,
                                            unsigned int maxGroupSize )
  : m_jtagPort( sca, maxGroupSize ),
    m_freqMhz( freqMhz )
{
}

// ----------------------------------------------------------------------------

XilinxJtagProgrammer::~XilinxJtagProgrammer()
{
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::programFromBitFile( const std::string& filename,
                                               int devs_before, int ibits_before,
                                               int devs_after, int ibits_after,
                                               int dev_instr )
{
  //! Will return false if the programming result was not OK,
  //! Will throw in case of an exceptional situation
  BitFile bitfile;
  bitfile.setFile( filename );

  bool reverseBytes = true;
  if( reverseBytes )
    bitfile.reverseBytes();

  if( !bitfile.valid() )
    throw std::runtime_error( "Bitfile invalid (TODO better exception)" );

  if( (devs_before > 0 && ibits_before > 0) ||
      (devs_after > 0 && ibits_after > 0) )
    return this->program( bitfile, devs_before, ibits_before,
                          devs_after, ibits_after, dev_instr );
  else
    return this->program( bitfile );
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::programFromBlob( const std::string& blob,
                                            int devs_before, int ibits_before,
                                            int devs_after, int ibits_after,
                                            int dev_instr )
{
  //! Will return false if the programming result was not OK,
  //! Will throw in case of an exceptional situation
  BitFile bitfile;
  bitfile.setBlob( blob );

  bool reverseBytes = true;
  if( reverseBytes )
    bitfile.reverseBytes();

  if( !bitfile.valid() )
    throw std::runtime_error( "Bitfile invalid (TODO better exception)" );

  if( (devs_before > 0 && ibits_before > 0) ||
      (devs_after > 0 && ibits_after > 0) )
    return this->program( bitfile, devs_before, ibits_before,
                          devs_after, ibits_after, dev_instr );
  else
    return this->program( bitfile );
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::program( BitFile& bitfile )
{
  m_jtagPort.configure( m_freqMhz );

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.showProgress( true );

  unsigned char instr;
  instr = V7_JPROGRAM;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 10000*m_freqMhz );

  instr = V7_CFG_IN;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.shiftDr( bitfile.nbytes()*8, bitfile.data() );

  m_jtagPort.showProgress( false );

  instr = V7_JSTART;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 2000*m_freqMhz );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.clearReadBack();

  m_jtagPort.shift( JtagPort::SHIFT_IR, V7_IRLEN, nullptr, /*read_tdi*/ true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply> replies = m_jtagPort.getReadBackFrames();
  if( replies.size() != 1 )
    scasw_throw_runtime_error_with_origin( "logic issue: expected 1 reply, got " +
                                           boost::lexical_cast<std::string>(replies.size()) );
  bool result;
  if( replies[0][6] == 0x35 )
    {
      result = true; // Programming was successful
    }
  else
    {
      LOG(Log::ERR) << "FPGA programming likely failed, reply frame: "
                    << replies[0].toString();
      result = false;
    }
  return result;
}

// ----------------------------------------------------------------------------

bool XilinxJtagProgrammer::program( BitFile& bitfile,
                                    int devs_before, int ibits_before,
                                    int devs_after, int ibits_after,
                                    int dev_instr )
{
  // Same function as program(bitfile), but now the FPGA is in a JTAG chain
  // with 'devs_before' JTAG devices *before* we get to the FPGA
  // and 'devs_after' JTAG devices *after* the FPGA, starting from the JTAG master;
  // 'ibits_before' and 'ibits_after' are the total number of instruction bits
  // in those devices, before and after the FPGA resp.;
  // if 'dev_instr' is given it is taken as the BYPASS instruction for each
  // individual device (so all the same..), and in that case 'ibits_before'
  // and 'ibits_after' are the number of instruction bits of *one* device.

  m_jtagPort.configure( m_freqMhz );

  // Go to a defined state
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.showProgress( true );

  unsigned char instr;
  instr = V7_JPROGRAM;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devs_before, ibits_before,
                      devs_after, ibits_after, dev_instr );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 10000*m_freqMhz );

  instr = V7_CFG_IN;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devs_before, ibits_before,
                      devs_after, ibits_after, dev_instr );
  m_jtagPort.shiftDr( bitfile.nbytes()*8, bitfile.data(),
                      /*read_tdi*/ false, devs_before, devs_after );

  m_jtagPort.showProgress( false );

  instr = V7_JSTART;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devs_before, ibits_before,
                      devs_after, ibits_after, dev_instr );
  m_jtagPort.shift( JtagPort::RUN_TEST_IDLE, 2000*m_freqMhz );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  m_jtagPort.clearReadBack();

  m_jtagPort.shiftIr( V7_IRLEN, 0, devs_before, ibits_before,
                      devs_after, ibits_after, dev_instr, /*read_tdi*/ true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  // The following only works correctly if the JTAG string
  // in the previous shiftIr() call is <=128 bits long
  size_t expected_size = 1, index = 0;;
  if( devs_before > 0 && ibits_before > 0 )
    {
      ++expected_size;
    }
  if( devs_after > 0 && ibits_after > 0 )
    {
      ++expected_size;
      ++index;
    }
  std::vector<Sca::Reply> replies = m_jtagPort.getReadBackFrames();
  if( replies.size() != expected_size )
    scasw_throw_runtime_error_with_origin( "logic issue: expected " +
                                           boost::lexical_cast<std::string>(expected_size) +
                                           " replies, got " +
                                           boost::lexical_cast<std::string>(replies.size()) );
  bool result;
  if( replies[index][6] == 0x35 )
    {
      result = true; // Programming was successful
    }
  else
    {
      LOG(Log::ERR) << "FPGA programming likely failed, reply frame: "
                    << replies[index].toString();
      result = false;
    }
  return result;
}

// ----------------------------------------------------------------------------

uint32_t XilinxJtagProgrammer::readId()
{
  m_jtagPort.configure( m_freqMhz );

  unsigned char instr = V7_IDCODE;
  m_jtagPort.shiftIr( V7_IRLEN, &instr );
  m_jtagPort.clearReadBack();

  m_jtagPort.shiftDr( V7_IDCODE_LEN, /*tdo*/ nullptr, /*read_tdi*/ true );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply> replies = m_jtagPort.getReadBackFrames();

  // Expect a single reply (previous JTAG string <= 128 bits long)
  if( replies.size() != 1 )
    scasw_throw_runtime_error_with_origin( "logic issue: expected 1 reply, got " +
                                           boost::lexical_cast<std::string>(replies.size()) );
  // The ID is in the reply
  uint32_t code = replies[0][5] << 24 | replies[0][4] << 16 | replies[0][7] << 8 | replies[0][6] ;
  return code;
}

// ----------------------------------------------------------------------------

uint32_t XilinxJtagProgrammer::readId( int devs_before, int ibits_before,
                                       int devs_after, int ibits_after,
                                       int dev_instr )
{
  // Same function as readId(), but now the FPGA is in a JTAG chain
  // with 'devs_before' JTAG devices *before* we get to the FPGA
  // and 'devs_after' JTAG devices *after* the FPGA, starting from the JTAG master;
  // 'ibits_before' and 'ibits_after' are the total number of instruction bits
  // in those devices, before and after the FPGA resp.;
  // if 'dev_instr' is given it is taken as the BYPASS instruction for each
  // individual device (so all the same..), and in that case 'ibits_before'
  // and 'ibits_after' are the number of instruction bits of *one* device.

  m_jtagPort.configure( m_freqMhz );

  unsigned char instr = V7_IDCODE;
  m_jtagPort.shiftIr( V7_IRLEN, instr, devs_before, ibits_before,
                      devs_after, ibits_after, dev_instr );

  m_jtagPort.clearReadBack();

  m_jtagPort.shiftDr( V7_IDCODE_LEN, /*tdo*/ nullptr, /*read_tdi*/ true,
                      devs_before, devs_after );
  m_jtagPort.gotoState( JtagPort::TEST_LOGIC_RESET );

  std::vector<Sca::Reply> replies = m_jtagPort.getReadBackFrames();

  // The following only works correctly if the JTAG string
  // in the previous shiftIr() call is <=128 bits long
  size_t expected_size = 1, index = 0;
  if( devs_before > 0 )
    {
      ++expected_size;
    }
  if( devs_after > 0 )
    {
      ++expected_size;
      ++index;
    }
  if( replies.size() != expected_size )
    scasw_throw_runtime_error_with_origin( "logic issue: expected " +
                                           boost::lexical_cast<std::string>(expected_size) +
                                           " replies, got " +
                                           boost::lexical_cast<std::string>(replies.size()) );
  // The ID is in one of the replies
  uint32_t code = (replies[index][5] << 24 | replies[index][4] << 16 |
                   replies[index][7] << 8 | replies[index][6]);
  return code;
}

// ----------------------------------------------------------------------------

std::string XilinxJtagProgrammer::idCodeToString( uint32_t id )
{
  std::stringstream output;
  output.fill('0');
  output.width(6);
  output << std::hex << id;
  // Xilinx 7-series FPGA family member IDs
  id = (id & 0x0FFFF000) >> 12;
  if( id == 0x3622 || id == 0x3620 || id == 0x37C4 ||
      id == 0x362F || id == 0x37C8 || id == 0x37C7 )
    output << " (Spartan-7)";
  else if( id == 0x37C3 || id == 0x362E || id == 0x37C2 ||
           id == 0x362D || id == 0x362C || id == 0x3632 ||
           id == 0x3631 || id == 0x3636 )
    output << " (Artix-7)";
  else if( id == 0x3647 || id == 0x364C || id == 0x3651 ||
           id == 0x3747 || id == 0x3656 || id == 0x3752 ||
           id == 0x3751 )
    output << " (Kintex-7)";
  else if( id == 0x3671 || id == 0x36B3 || id == 0x3667 ||
           id == 0x3682 || id == 0x3687 || id == 0x3692 ||
           id == 0x3691 || id == 0x3696 || id == 0x36D5 ||
           id == 0x36D9 || id == 0x36DB )
    output << " (Virtex-7)";
  else if( id == 0x0000 || id == 0xFFFF )
    output << " (no device connected?)";
  else
    output << " (<unknown>)";
  return output.str();
}

// ----------------------------------------------------------------------------
} /* namespace HenksJtag */
