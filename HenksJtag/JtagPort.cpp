/* Henk Boterenbrood is the original author of this file.
 * Brought to SCA-SW by Piotr Nikiel, June 2018.
 *
 * Revised Nov 2019, Henk B.
 */

#include <iostream>
#include <iomanip>
#include <thread> // For this_thread::sleep_for

#include <HenksJtag/JtagPort.h>

// GBT-SCA device identifiers (Channel byte)
#define SCA_DEV_CONFIG         0x00
#define SCA_DEV_JTAG           0x13

// SCA_DEV_CONFIG commands
#define SCA_CONFIG_WR_A        0x00
#define SCA_CONFIG_RD_A        0x01
#define SCA_CONFIG_WR_B        0x02
#define SCA_CONFIG_RD_B        0x03
#define SCA_CONFIG_WR_C        0x04
#define SCA_CONFIG_RD_C        0x05
#define SCA_CONFIG_WR_D        0x06
#define SCA_CONFIG_RD_D        0x07

// SCA_DEV_JTAG commands
#define SCA_JTAG_WR_CTRL       0x80
#define SCA_JTAG_RD_CTRL       0x81
#define SCA_JTAG_WR_FREQ       0x90
#define SCA_JTAG_RD_FREQ       0x91
#define SCA_JTAG_WR_TDO0       0x00
#define SCA_JTAG_WR_TDO1       0x10
#define SCA_JTAG_WR_TDO2       0x20
#define SCA_JTAG_WR_TDO3       0x30
#define SCA_JTAG_RD_TDI0       0x01
#define SCA_JTAG_RD_TDI1       0x11
#define SCA_JTAG_RD_TDI2       0x21
#define SCA_JTAG_RD_TDI3       0x31
#define SCA_JTAG_WR_TMS0       0x40
#define SCA_JTAG_WR_TMS1       0x50
#define SCA_JTAG_WR_TMS2       0x60
#define SCA_JTAG_WR_TMS3       0x70
#define SCA_JTAG_RD_TMS0       0x41
#define SCA_JTAG_RD_TMS1       0x51
#define SCA_JTAG_RD_TMS2       0x61
#define SCA_JTAG_RD_TMS3       0x71
#define SCA_JTAG_ARESET        0xC0
#define SCA_JTAG_GO            0xA2
#define SCA_JTAG_GO_MANUAL     0xB0

namespace HenksJtag
{
const int JTAG_WR[] = { SCA_JTAG_WR_TDO0, SCA_JTAG_WR_TDO1,
                        SCA_JTAG_WR_TDO2, SCA_JTAG_WR_TDO3 };
const int JTAG_RD[] = { SCA_JTAG_RD_TDI0, SCA_JTAG_RD_TDI1,
                        SCA_JTAG_RD_TDI2, SCA_JTAG_RD_TDI3 };

// ----------------------------------------------------------------------------

JtagPort::JtagPort( Sca::Sca& sca, unsigned int maxGroupSize )
  : _state( TEST_LOGIC_RESET ),
    _progress( false ),
    _ctrlReg( 0x0000 ),
    _goDelayFactor( 20 ), // Matches 1MHz JTAG clock
    m_sca( sca ),
    m_synchronousService( sca.synchronousService() ),
    m_maxGroupSize( maxGroupSize )
{
  // Control Register bit 11 'MSB/LSB' (0x0800) here (above) initialised to 0;
  // GBT-SCA docu (v8.2-draft) says "bits transmitted msb to lsb" if 0,
  // but the opposite is true (Henk B, 23 Nov 2017)

  m_groupedRequests.reserve( 256 );
}

// ----------------------------------------------------------------------------

JtagPort::~JtagPort()
{
}

// ----------------------------------------------------------------------------

void JtagPort::configure( int freq_mhz )
{
  uint8_t data[4];

  resetScaFrames();

  // Configure GBT-SCA JTAG channel (standard; LEN=128 (represented by value 0)
  _ctrlReg &= ~0x007F;
  mapInt2Bytes( _ctrlReg, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );
  // Frequency of 20MHz: f=2*10000000/(DIV+1) --> DIV=0
  // (NB: LSByte is in data[2] and MSByte in data[3])
  uint8_t div = 19; // Default 1MHz
  if( freq_mhz == 20 )
    div = 0;
  else if( freq_mhz == 10 )
    div = 1;
  else if( freq_mhz == 5 )
    div = 3;
  else if( freq_mhz == 4 )
    div = 4;
  else if( freq_mhz == 2 )
    div = 9;
  else if( freq_mhz == 1 )
    div = 19;
  else
    scasw_throw_runtime_error_with_origin("Invalid bitrate. Must be from the set {20,10,5,4,2,1} MHz}");
  _goDelayFactor = 20/freq_mhz;
  memset( data, 0, 4 );
  data[2] = div;
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_FREQ, data );

  uploadScaFrames();

  std::this_thread::sleep_for( std::chrono::milliseconds(1) );

  resetScaFrames();

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  uploadScaFrames();
}

// ----------------------------------------------------------------------------

void JtagPort::gotoState( state_t s )
{
  uint32_t tms;
  int      nbits = setState( s, &tms );
  //std::cout << "state " << s << ": "
  //          << nbits << " bits, tms=" << tms << std::endl;
  if( nbits <= 0 ) return;

  resetScaFrames();

  // Configure the number of bits
  uint8_t data[4];
  _ctrlReg &= ~0x007F;
  _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  // Configure TDO and TMS
  memset( data, 0, 4 );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO0, data );
  mapInt2Bytes( tms, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );

  // Shift the bits (delay divided by 4: not more than 32 bits instead of 128)
  memset( data, 0, 4 );
  addScaFrame( SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, (_goDelayFactor*8)/4  );

  uploadScaFrames();
}

// ----------------------------------------------------------------------------

void JtagPort::shiftIr( int nbits, uint8_t *tdo )
{
  shift( SHIFT_IR, nbits, tdo );
}

// ----------------------------------------------------------------------------

void JtagPort::shiftDr( int nbits, uint8_t *tdo, bool read_tdi )
{
  shift( SHIFT_DR, nbits, tdo, read_tdi );
}

// ----------------------------------------------------------------------------

void JtagPort::shiftIr( int irlen,       int instr,
                        int devs_before, int ibits_before,
                        int devs_after,  int ibits_after,
                        int dev_instr,   bool read_tdi )
{
  bool final = false;
  if( devs_after > 0 && ibits_after > 0 )
    {
      // Insert BYPASS instruction for the devices present in the JTAG chain
      // *after* the selected device
      if( dev_instr >= 0 )
        {
          // SPECIAL:
          // assume the BYPASS instruction is 'dev_instr' for each device
          // with instruction length 'ibits_after'
          std::vector<uint8_t> bypass( (devs_after*ibits_after+7)/8, 0x00 );
          int bit = 0;
          for( int dev=devs_after-1; dev>= 0; --dev )
            {
              for( int i=0; i<ibits_after; ++i, ++bit )
                if( dev_instr & (1<<i) )
                  bypass[bit/8] |= (1 << (bit & 0x7));
            }
          shift( SHIFT_IR, devs_after*ibits_after, bypass.data(), false, final );
        }
      else
        {
          // Assume the BYPASS instruction is all 1s
          // independent of individual instruction sizes
          // and 'ibits_after' is the *total* number of instruction bits
          std::vector<uint8_t> bypass( (ibits_after+7)/8, 0xFF );
          shift( SHIFT_IR, ibits_after, bypass.data(), false, final );
        }
    }

  // Shift instruction into the selected device
  final = (devs_before <= 0);
  uint8_t *tdo = (uint8_t *) &instr;
  shift( SHIFT_IR, irlen, tdo, read_tdi, final );

  if( devs_before > 0 && ibits_before > 0 )
    {
      // Insert BYPASS instruction for the devices present in the JTAG chain
      // *before* the selected device
      if( dev_instr >= 0 )
        {
          // SPECIAL:
          // assume the BYPASS instruction is 'dev_instr' for each device
          // with instruction length 'ibits_before'
          std::vector<uint8_t> bypass( (devs_before*ibits_before+7)/8, 0x00 );
          int bit = 0;
          for( int dev=devs_before-1; dev>= 0; --dev )
            {
              for( int i=0; i<ibits_before; ++i, ++bit )
                if( dev_instr & (1<<i) )
                  bypass[bit/8] |= (1 << (bit & 0x7));
            }
          shift( SHIFT_IR, devs_before*ibits_before, bypass.data(), false, final );
        }
      else
        {
          // Assume the BYPASS instruction is all 1s
          // independent of individual instruction sizes
          // and 'ibits_after' is the *total* number of instruction bits
          std::vector<uint8_t> bypass( (ibits_before+7)/8, 0xFF );
          shift( SHIFT_IR, ibits_before, bypass.data(), false, final );
        }
    }
}

// ----------------------------------------------------------------------------

void JtagPort::shiftDr( int nbits, uint8_t *tdo, bool read_tdi,
                        int devs_before, int devs_after )
{
  bool final = false;
  if( devs_after > 0 )
    {
      // BYPASS 1-bit per device for the devices *after* the selected device
      std::vector<uint8_t> bypass( (devs_after+7)/8, 0xFF );
      shift( SHIFT_DR, devs_after, bypass.data(), false, final );
    }

  // Shift data into the selected device(s)
  final = (devs_before <= 0);
  shift( SHIFT_DR, nbits, tdo, read_tdi, final );

  if( devs_before > 0 )
    {
      // BYPASS 1-bit per device for the devices *before* the selected device
      std::vector<uint8_t> bypass( (devs_before+7)/8, 0xFF );
      final = true;
      shift( SHIFT_DR, devs_before, bypass.data(), false, final );
    }
}

// ----------------------------------------------------------------------------

void JtagPort::shift( state_t state, int nbits, uint8_t *tdo,
                      bool read_tdi, bool final )
{
  if( nbits <= 0 ) return;

  //if( state == SHIFT_IR || state == SHIFT_DR )
  gotoState( state );

  resetScaFrames();

  // Reset GBT-SCA JTAG TDO and TMS registers
  addResetTdoTms();

  uint8_t data[4];
  if( nbits > 128 )
    {
      // Configure the number of bits: 128
      _ctrlReg &= ~0x007F;
      mapInt2Bytes( _ctrlReg, data );
      addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );
    }

  uploadScaFrames();

  int bytes_todo = nbits/8;
  int bytes_done = 0;
  int percentage = 0;
  if( _progress && state == SHIFT_DR )
    std::cout << "Uploading...     ";

  int index = 0;
  while( nbits > 128 )
    {
      resetScaFrames();

      // Accumulate up to 'max_cnt' groups of commands into a single DMA
      int max_cnt = 50, cnt = 0;
      while( cnt < max_cnt && nbits > 128 )
        {
          if( tdo )
            {
              for( int i=0; i<4; ++i )
                {
                  data[2] = tdo[index++];
                  data[3] = tdo[index++];
                  data[0] = tdo[index++];
                  data[1] = tdo[index++];
                  addScaFrame( SCA_DEV_JTAG, 4, JTAG_WR[i], data );
                }
            }

          // Shift the bits (delay required for 128 bits)
          memset( data, 0, 4 );
          addScaFrame( SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelayFactor*8 );

          if( read_tdi )
            {
              for( int i=0; i<4; ++i )
                // Delay of 1 us required !
                addScaFrame( SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1, read_tdi );
            }

          nbits -= 128;
          bytes_done += 16;
          ++cnt;
        }

      uploadScaFrames();

      if( _progress && state == SHIFT_DR )
        // Display percentage done
        displayPercentage( &percentage, bytes_done, bytes_todo );
    }

  resetScaFrames();

  // Final bits (128 bits or less)
  _ctrlReg &= ~0x007F;
  if( nbits < 128 )
    _ctrlReg |= (nbits & 0x7F);
  mapInt2Bytes( _ctrlReg, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_CTRL, data );

  if( tdo )
    {
      for( int i=0; i<4; ++i )
        if( nbits > i*32 )
          {
            data[2] = tdo[index++];
            data[3] = tdo[index++];
            data[0] = tdo[index++];
            data[1] = tdo[index++];
            addScaFrame( SCA_DEV_JTAG, 4, JTAG_WR[i], data );
          }
    }

  // Configure TMS: exit SHIFT_DR or SHIFT_IR state on the final bit;
  // all other states are done with TMS=0 only
  uint32_t tms = 0;
  if( final && (state == SHIFT_IR || state == SHIFT_DR) )
    tms = (1 << ((nbits-1) & 0x1F));
  if( tms != 0 )
    {
      mapInt2Bytes( tms, data );
      if( nbits <= 32 )
        addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );
      else if( nbits <= 64 )
        addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS1, data );
      else if( nbits <= 96 )
        addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS2, data );
      else
        addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS3, data );
    }

  // Shift the bits (delay required for 128 bits)
  memset( data, 0, 4 );
  addScaFrame( SCA_DEV_JTAG, 2, SCA_JTAG_GO, data, _goDelayFactor*8 );

  if( read_tdi )
    {
      for( int i=0; i<4; ++i )
        if( nbits > i*32 )
          // Delay of 1 us required !
          addScaFrame( SCA_DEV_JTAG, 0, JTAG_RD[i], data, 1, read_tdi );
    }

  uploadScaFrames();

  if( _progress && bytes_todo > 0 && state == SHIFT_DR )
    {
      bytes_done += (nbits+7)/8;
      // Display percentage done
      displayPercentage( &percentage, bytes_done, bytes_todo );
    }

  if( final && (state == SHIFT_IR || state == SHIFT_DR) )
    {
      // We left the SHIFT_DR or SHIFT_IR state
      if( state == SHIFT_DR )
        _state = EXIT1_DR;
      else
        _state = EXIT1_IR;
      // Continue to state RUN_TEST_IDLE
      gotoState( RUN_TEST_IDLE );
    }
}

// ----------------------------------------------------------------------------

int JtagPort::setState( state_t s, uint32_t *tms )
{
  // Do nothing for invalid state
  if( s > UPDATE_IR )
    return 0;

  int nbits = 0;
  *tms = 0x0;

  // Reset (from any state in at most 5 clock cycles with TMS=1)
  if( s == TEST_LOGIC_RESET )
    {
      *tms = 0x1F;
      _state = TEST_LOGIC_RESET;
      nbits = 5;
    }

  // Go to the requested TAP state step by step,
  // using the shortest possible path in the state diagram
  while( _state != s )
    {
      if( _state == TEST_LOGIC_RESET && _state != s )
        {
          // TMS=0
          ++nbits; 
          _state = RUN_TEST_IDLE;
        }
      if( _state == RUN_TEST_IDLE && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = SELECT_DR_SCAN;
        }
      if( _state == SELECT_DR_SCAN && _state != s )
        {
          if( s < TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_IR_SCAN;
            }
          ++nbits;
        }
      if( _state == CAPTURE_DR && _state != s )
        {
          if( s == SHIFT_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_DR;
            }
          ++nbits;
        }
      if( _state == SHIFT_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_DR;
        }
      if( _state == EXIT1_DR && _state != s )
        {
          if( s == PAUSE_DR ||
              s == EXIT2_DR )
            {
              // TMS=0
              _state = PAUSE_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == PAUSE_DR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT2_DR;
        }
      if( _state == EXIT2_DR && _state != s )
        {
          if( s == SHIFT_DR ||
              s == EXIT1_DR ||
              s == PAUSE_DR )
            {
              // TMS=0
              _state = SHIFT_DR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_DR;
            }
          ++nbits;
        }
      if( _state == UPDATE_DR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
      if( _state == SELECT_IR_SCAN && _state != s )
        {
          if( s != TEST_LOGIC_RESET )
            {
              // TMS=0
              _state = CAPTURE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = TEST_LOGIC_RESET;
            }
          ++nbits;
        }
      if( _state == CAPTURE_IR && _state != s )
        {
          if( s == SHIFT_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = EXIT1_IR;
            }
          ++nbits;
        }
      if( _state == SHIFT_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits;
          _state = EXIT1_IR;
        }
      if( _state == EXIT1_IR && _state != s )
        {
          if( s == PAUSE_IR ||
              s == EXIT2_IR )
            {
              // TMS=0
              _state = PAUSE_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == PAUSE_IR && _state != s )
        {
          *tms |= (1 << nbits); // TMS=1
          ++nbits; 
          _state = EXIT2_IR;
        }
      if( _state == EXIT2_IR && _state != s )
        {
          if( s == SHIFT_IR  ||
              s == EXIT1_IR  ||
              s == PAUSE_IR )
            {
              // TMS=0
              _state = SHIFT_IR;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = UPDATE_IR;
            }
          ++nbits;
        }
      if( _state == UPDATE_IR && _state != s )
        {
          if( s == RUN_TEST_IDLE )
            {
              // TMS=0
              _state = RUN_TEST_IDLE;
            }
          else
            {
              *tms |= (1 << nbits); // TMS=1
              _state = SELECT_DR_SCAN;
            }
          ++nbits;
        }
    }
  return nbits;
}

// ----------------------------------------------------------------------------

void JtagPort::addResetTdoTms()
{
  // Reset GBT-SCA JTAG TDO and TMS registers
  uint8_t data[4] = { 0, 0, 0, 0 };
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO0, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO1, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO2, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TDO3, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS0, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS1, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS2, data );
  addScaFrame( SCA_DEV_JTAG, 4, SCA_JTAG_WR_TMS3, data );
}

// ----------------------------------------------------------------------------

void JtagPort::mapInt2Bytes( uint32_t i, uint8_t *bytes )
{
  bytes[2] = (uint8_t) ((i >>  0) & 0xFF);
  bytes[3] = (uint8_t) ((i >>  8) & 0xFF);
  bytes[0] = (uint8_t) ((i >> 16) & 0xFF);
  bytes[1] = (uint8_t) ((i >> 24) & 0xFF);
}

// ----------------------------------------------------------------------------

void JtagPort::displayPercentage( int *percentage,
                                  int  bytes_done,
                                  int  bytes_todo )
{
  // Display percentage done (on change of integer value)
  int p = *percentage;
  double pf = ((double) 100.0*bytes_done)/(double) bytes_todo;
  if( (int) pf != p )
    {
      p = (int) pf;
      // Overwrite currently displayed percentage value
      std::cout << "\b\b\b\b\b    \b\b\b\b" << std::setw(3) << p << "% ";
      std::cout.flush();
      *percentage = p;
    }
}

// ----------------------------------------------------------------------------

bool JtagPort::addScaFrame( int          chan, int len, int cmd,
                            uint8_t     *data,
                            unsigned int delay_us, bool store_readback )
{
  Sca::Request request( chan, cmd, data, len );
  request.setTrailingPause( delay_us );
  if( store_readback )
    {
      // Here we really have to flush out ...
      LOG(Log::TRC) << "Flushing queue because readback was requested";

      uploadScaFrames();
      Sca::Reply reply = m_synchronousService.sendAndWaitReply( request, 1000 );
      m_readBackFrames.push_back( reply );
      throwIfScaReplyError( reply );

      LOG(Log::INF) << "Readback frame was:" << reply.toString();
    }
  else
    {
      if( m_groupedRequests.size() >= m_maxGroupSize )
        uploadScaFrames();
      m_groupedRequests.push_back( request );
      LOG(Log::TRC) << "Pushed back this frame:" << request.toString();
    }
  return true;
}

// ----------------------------------------------------------------------------

bool JtagPort::uploadScaFrames()
{
  if( m_groupedRequests.size() > 0 )
    {
      LOG(Log::TRC) << "Will flush " << m_groupedRequests.size() << " frames";

      std::vector<Sca::Reply> replies =
        m_synchronousService.sendAndWaitReply( m_groupedRequests, 1000 );

      for( Sca::Reply &r: replies )
        throwIfScaReplyError( r );

      resetScaFrames();
    }
  return true;
}

// ----------------------------------------------------------------------------

void JtagPort::resetScaFrames()
{
  m_groupedRequests.clear();
  m_groupedRequests.reserve( 256 );
}

// ----------------------------------------------------------------------------
} // namespace HenksJtag
